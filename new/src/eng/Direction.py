from enum import Enum

class Direction(Enum):
    """
    Enum class of possible direction's.
    up, down, right, left or none (undefined)
    """
    ## @var up 
    # Direction UP
    up = 8
    ## @var down 
    # Direction DOWN
    down = 2
    ## @var right 
    # Direction RIGHT
    right = 6
    ## @var left 
    # Direction LEFT
    left = 4
    ## @var none 
    # Direction NONE
    none = 255

def opposite_direction(direct):
    """
    Change direction to opposite
    @param direct
    @return opposite direction
    """
    if direct == Direction.up:
        return Direction.down
    if direct == Direction.down:
        return Direction.up
    if direct == Direction.right:
        return Direction.left
    if direct == Direction.left:
        return Direction.right