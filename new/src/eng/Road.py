import eng.Tiles as Tiles
import eng.TrafficLight as Traffic
import eng.Direction as Direction

class Road:
    """
    Class of Road
    """

    def __init__(self):
        """
        Constructor
        Create static map and objects on it.
        """

        ## @var grid
        #  Grid of tiles
        self.grid = []
        
        for i in range(50):
            temp = []
            for j in range(50):
                temp.append(Tiles.Tiles())
            self.grid.append(temp)

        # --- Główne skrzyżowanie (dwupasmowe) ---
        for i in range(50):
            self.grid[25][i].tile_type = "street"
            self.grid[25][i].road_directions = [Direction.Direction.right]

            self.grid[24][i].tile_type = "street"
            self.grid[24][i].road_directions = [Direction.Direction.left]

            self.grid[i][25].tile_type = "street"
            self.grid[i][25].road_directions = [Direction.Direction.up]

            self.grid[i][24].tile_type = "street"
            self.grid[i][24].road_directions = [Direction.Direction.down]


        # Dodatkowa droga
        for i in range(23, -1, -1):
            self.grid[i][10].tile_type = "street"
            self.grid[i][10].road_directions = [Direction.Direction.up]
        self.grid[24][10].is_crossing = True
        self.grid[24][10].road_directions = [ Direction.Direction.left,  Direction.Direction.up]
        self.grid[0][10].end_point = True

        # Ustawienie skrzyzowania
        self.grid[24][24].is_crossing = True
        self.grid[24][24].road_directions = [Direction.Direction.down, Direction.Direction.left, Direction.Direction.right]
        # Configure Traffic light
        self.grid[24][24].traffic_light = Traffic.traffic_light(24, 24)
        self.grid[24][24].traffic_light.direction = Direction.Direction.down
        # X visualisation position
        self.grid[24][24].traffic_light.x_vis = 240
        # Y visualisation position
        self.grid[24][24].traffic_light.y_vis = 200
        # Change traffic light color to green
        self.grid[24][24].traffic_light.picked_pattern = self.grid[24][24].traffic_light.first_pattern

        self.grid[24][25].is_crossing = True
        self.grid[24][25].traffic_light =  Traffic.traffic_light(24, 25)
        self.grid[24][25].traffic_light.direction = Direction.Direction.left
        self.grid[24][25].road_directions = [Direction.Direction.down, Direction.Direction.left, Direction.Direction.up]
        # X visualisation position
        self.grid[24][25].traffic_light.x_vis = 330
        # Y visualisation position
        self.grid[24][25].traffic_light.y_vis = 200

        self.grid[25][25].is_crossing = True
        self.grid[25][25].traffic_light = Traffic.traffic_light(25, 25)
        self.grid[25][25].traffic_light.direction = Direction.Direction.up
        self.grid[25][25].road_directions = [Direction.Direction.up, Direction.Direction.left, Direction.Direction.right]
        # X visualisation position
        self.grid[25][25].traffic_light.x_vis = 330
        # Y visualisation position
        self.grid[25][25].traffic_light.y_vis = 320
        # Change traffic light color to green
        self.grid[25][25].traffic_light.picked_pattern = self.grid[25][25].traffic_light.first_pattern

        self.grid[25][24].is_crossing = True
        self.grid[25][24].traffic_light = Traffic.traffic_light(25, 24)
        self.grid[25][24].traffic_light.direction = Direction.Direction.right
        self.grid[25][24].road_directions = [Direction.Direction.down, Direction.Direction.up, Direction.Direction.right]
        # X visualisation position
        self.grid[25][24].traffic_light.x_vis = 240
        # Y visualisation position
        self.grid[25][24].traffic_light.y_vis = 320

        # Ustawienie spawn pointow i end pointow

        self.grid[0][24].spawn_point = True
        self.grid[0][25].end_point = True

        self.grid[25][0].spawn_point = True
        self.grid[24][0].end_point = True

        self.grid[49][25].spawn_point = True
        self.grid[49][24].end_point = True

        self.grid[24][49].spawn_point = True
        self.grid[25][49].end_point = True
        

    def show(self, car_array):
        """
        Print map in console with cars.
        @param car_array Array of cars
        """
        temp = ""
        for i in range(len(self.grid)):
            for j in range(len(self.grid[i])):
                car_in_this_position = False
                for car in car_array:
                    if car.i == i and car.j == j:
                        temp += str(car)
                        car_in_this_position = True
                        break
                if car_in_this_position == False:
                    temp += str(self.grid[i][j])
            temp += '\n'
        print(temp)