import random
import numpy as np

class Rnd:
    """
    Rnd class which help to randomize events
    """

    def __random_chance(self, chance):
        """
        Random value between 0 and 1.
        When value is <= than chance return True
        otherwise return False
        @param chance A chance to make an event happen
        @return Did the event happen? True/False
        """
        return np.random.rand() <= chance

    def speed_up(self, chance):
        """
        Speed up event
        @param chance A chance to make an event happen
        @return Did the event happen? True/False
        """
        return self.__random_chance(chance)

    def slow_down(self, chance):
        """
        Slow down event
        @param chance A chance to make an event happen
        @return Did the event happen? True/False
        """
        return self.__random_chance(chance)

    def spawn_car(self, chance):
        """
        Spawn car event
        @param chance A chance to make an event happen
        @return Did the event happen? True/False
        """
        return self.__random_chance(chance)

    def rule_break(self, chance):
        """
        Rule break event
        @param chance A chance to make an event happen
        @return Did the event happen? True/False
        """
        return self.__random_chance(chance)

    def random_pick(self, array):
        """
        @param array Some array of elements
        @return Random picked element
        """
        return random.choice(array)

    def rand_range(self, start, stop):
        """
        @param start Minimal value
        @param stop Maximal value
        @return Random value between start and stop
        """
        return random.randrange(start, stop)