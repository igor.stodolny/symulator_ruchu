import random
import eng.Direction as Direction


## @var cars_colors
# Array of possible colors of our cars
cars_colors = [(255, 0, 255), (0, 255, 255), (255,255,0), (255,250,205), (244, 164, 96),(135, 206, 250)]
## @var last_color
# Last used color (To prevent from spawning two same color cars near each other)
last_color = 0

def gen_color():
    """
    Get random color of cars, but it's different than previous one.
    @return Random color
    """
    global last_color
    new_color = cars_colors[random.randint(0, len(cars_colors) - 1)]
    while new_color == last_color:
        new_color = cars_colors[random.randint(0, len(cars_colors) - 1)]
    last_color = new_color
    return new_color

class Car:
    def __init__(self, i, j, drive_direction , vel = 0):
        """
        Constructor of class Car
        @param i Position of car on Y axis
        @param j Position of car on X axis
        @param drive_direction Drive direction (Must be instance of Direction class in Direction.py)
        @param vel Car velocity
        """
        assert isinstance(vel, int), "Velocity must be an int"
        assert isinstance(i, int), "i must be an int"
        assert isinstance(j, int), "j must be an int"
        assert isinstance(drive_direction, Direction.Direction) , "Direction must be an class enum direction"


        ## @var color
        # Car color
        self.color: int = gen_color()

        ## @var i
        # Car i position (Y Axis)
        self.i: int = i

        ## @var j
        # Car j position (X Axis)
        self.j: int = j

        ## @var vel
        # Car velocity
        self.vel = vel

        ## @var drive_direction
        # Car driving direction
        self.drive_direction = drive_direction
        
        ## @var TTL
        # Time To Live
        self.TTL = 0
        self.stop = 0

    def __del__(self):
        """
        Destructor of class Car
        Save final statistic of car
        """
        f = open("stats.txt", "a")
        f.write(str(self.TTL) + ";" + str(self.stop) + '\n')
        f.close();

    def move(self):
        """
        Move car in one of fourth direction.
        """
        self.TTL= self.TTL + 1

        if self.vel == 0:
            self.stop = self.stop + 1


        if self.drive_direction == Direction.Direction.right:
            self.j += self.vel
        elif self.drive_direction == Direction.Direction.left:
            self.j -= self.vel
        elif self.drive_direction == Direction.Direction.up:
            self.i -= self.vel
        elif self.drive_direction == Direction.Direction.down:
            self.i += self.vel

    def get_opposite_direction(self):
        """
        Change car direction to opposite.
        @return Opposite direction
        """
        return Direction.opposite_direction(self.drive_direction);

    def __repr__(self):
        """
        Get string representation of car.
        This representation is velocity:
        vel == 4 -> "4"
        vel == 3 -> "3"
        etc...
        @return String representation
        """
        return str(self.vel)

    def get_velocity(self):
        """
        Get current velocity of car
        @return Current velocity
        """
        return self.vel
    
    def set_velocity(self, new_velocity):
        """
        Set new velocity
        @param new_velocity Value of new velocity
        """
        assert isinstance(new_velocity, int), "Velocity must be an int"
        self.vel = new_velocity

    def get_direction(self):
        """
        Get current drive direction
        @return Current drive direction
        """
        return self.drive_direction
    
    def set_direction(self, new_direction):
        """
        Change driving direction of car
        @param new_direction New drive direction (Must be instance of Direction class in Direction.py)
        """
        assert isinstance(new_direction, Direction.Direction) , "Direction must be an class enum direction"
        self.drive_direction = new_direction