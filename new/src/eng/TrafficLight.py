
class traffic_light:
    """
    Traffic light class
    """

    def __init__(self, x, y):
        """
        Constructor
        @param x Position on map (X axis)
        @param y Position on map (Y axis)
        """
        ## @var tick_counter
        # Tick counter - Actual tick
        self.tick_counter = 0

        # 10 tick, 2 tick, 10 tick
        ## @var pattern_tick
        # Light pattern
        self.pattern_tick = [10, 5, 10, 5]

        ## @var first_pattern
        # Light first color pattern
        self.first_pattern = ["green", "red", "red", "red"]

        ## @var second_pattern
        # Light second color pattern
        self.second_pattern = ["red", "red", "green", "red"]

        # 0 - First pattern
        # 1 - Second pattern
        ## @var picked_pattern
        # Picked pattern (0 or 1)
        self.picked_pattern = self.second_pattern

        ## @var pattern_position
        # Pattern position 0 or 1
        self.pattern_position = 0

        ## @var x
        # x position on grid (j)
        self.x = x

        ## @var y
        # y position on grid (i)
        self.y = y

        ## @var x_vis
        # x visualisation position
        self.x_vis = 0

        ## @var y_vis
        # y visualisation position
        self.y_vis = 0

    def get_color(self):
        """
        Get color of current traffic light
        @return Traffic light color
        """
        return self.picked_pattern[self.pattern_position]

    def tick(self):
        """
        Change traffic light color after some ticks.
        Depends on pattern_tick array.
        """
        self.tick_counter += 1
        if self.tick_counter % self.pattern_tick[self.pattern_position] == 0:
            self.tick_counter = 0
            self.pattern_position = ( self.pattern_position + 1 ) % (len(self.pattern_tick))
        
