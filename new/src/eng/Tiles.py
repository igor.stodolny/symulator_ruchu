class Tiles:
    """
    Class of Tiles.
    Tiles is one single block in the map (Road)
    """

    def __init__(self, tile_type = "grass", road_directions = []):
        """
        Constructor
        @param tile_type Can be grass or street
        @param road_directions All possible direction to drive from this tile
        """

        ## @var tile_type
        #  Tile type
        self.tile_type = tile_type

        ## @var road_directions
        # Road direction (array)
        self.road_directions = road_directions

        ## @var traffic_light
        # Tile is traffic light? No -> None, Yes -> Traffic light instance
        self.traffic_light = None

        ## @var spawn_point
        # Is spawn point? True/False
        self.spawn_point = False

        ## @var end_point
        # Is end point? True/False
        self.end_point = False
        
        ## @var is_crossing
        # Is crossing? True/False
        self.is_crossing = False

        ## @var is_obstacle
        # Is obstacle? True/False
        self.is_obstacle = False
        
    def __repr__(self):
        """
        String representation of tile
        @return Representation in string
        """
        if self.is_crossing:
            return "X"
        elif self.spawn_point == True:
            return 'S'
        elif self.end_point == True:
            return 'E'
        elif self.is_obstacle == True:
            return '#'
        elif len(self.road_directions) == 1:
            if self.road_directions[0] == "right":
                return '>'
            if self.road_directions[0] == "left":
                return '<'
            if self.road_directions[0] == "up":
                return '^'
            if self.road_directions[0] == "down":
                return 'v'
        if self.tile_type == "grass":
            return '.'
        
        return self.tile_type[0].upper()