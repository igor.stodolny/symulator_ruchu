import eng.Road as Road
import eng.Car as Car
import eng.Direction as Direction
import eng.Rnd as Rnd

class Motion:
    """
    Class is responsible for moving car in map.
    Class contains all cars (List) and road
    """

    def __init__(self):
        """
        Constructor.
        Initialise cars and road
        """
        ## @var rnd 
        # Rnd Class (Rnd.py)
        self.rnd = Rnd.Rnd()

        ## @var cars
        # List of cars
        self.cars = []

        ## @var road
        # Road class (Road.py)
        self.road = Road.Road()

        ## @var v_max
        # Car max velocity
        self.v_max = 4 # 10->144km/h, 9->130km/h, 8->115km/h, 7->101km/h, 6->86km/h,
          # 5->72km/h,   4->57km/h,  3->43km/h,  2->29km/h,  1->14km/h

        # Chance to random event in %
        ## @var showup_chance
        # Car showup chance
        self.showup_chance = 1

        ## @var slowdown_chance
        # Car slowdown chance
        self.slowdown_chance = 0.1

        ## @var speedup_chance
        # Car speedup chance
        self.speedup_chance = 0.4

        ## @var rulebreak_chance
        # Car rule break chance
        self.rulebreak_chance = 0.1

    def spawn(self):
        """
        Spawn car in one of the free spawn points.
        Ratio of spawning car depends on variable showup_chance
        """
        spawn_points = []    
        for i in range(len(self.road.grid)):
            for j in range(len(self.road.grid[i])):
                if self.road.grid[i][j].spawn_point == True:
                    spawn_points.append((i,j))

        point = self.rnd.random_pick(spawn_points)
        if self.rnd.spawn_car(self.showup_chance):
            if Direction.Direction.right in self.road.grid[point[0]][point[1]].road_directions:
                direct = Direction.Direction.right
            if Direction.Direction.left in self.road.grid[point[0]][point[1]].road_directions:
                direct = Direction.Direction.left
            if Direction.Direction.up in self.road.grid[point[0]][point[1]].road_directions:
                direct = Direction.Direction.up
            if Direction.Direction.down in self.road.grid[point[0]][point[1]].road_directions:
                direct = Direction.Direction.down
            car_exists = False
            for car in self.cars:
                if point[0] == car.i and point[1] == car.j:
                    car_exists = True
                    break
            if not car_exists: 
                self.cars.insert(0, Car.Car(point[0], point[1], drive_direction=direct, vel=self.rnd.rand_range(1, self.v_max+1)))

    def nagel_right(self):
        """
        Try to move cars to the right
        @return Moved cars as array
        """
        # Create vector of cars driving to right
        cars_right = []
        for i in self.cars:
            if i.drive_direction == Direction.Direction.right:
                cars_right.append(i)
        cars_right.sort(key=lambda x: x.j)
        # Iterate over all car
        for i in range(len(cars_right)-1,-1,-1):
            # If direction != right continue
            if cars_right[i].drive_direction != Direction.Direction.right:
                continue


            # Random events
            if self.rnd.rule_break(self.rulebreak_chance) and (cars_right[i].vel >= self.v_max and cars_right[i].vel < 10):
                cars_right[i].vel += 1
            if self.rnd.speed_up(self.speedup_chance * ((1/(cars_right[i].vel + 1)) + 1.5)) and cars_right[i].vel < self.v_max or cars_right[i].vel == 0:
                cars_right[i].vel += 1
            if self.rnd.slow_down(self.slowdown_chance) and cars_right[i].vel > 1:
                cars_right[i].vel -= 1

            new_vel = cars_right[i].vel

            # Check for traffic light
            for k in range(cars_right[i].j + 1, cars_right[i].j+cars_right[i].vel+1):
                if k>=len(self.road.grid[i]):
                    break
                if self.road.grid[cars_right[i].i][k].traffic_light != None and cars_right[i].drive_direction == self.road.grid[cars_right[i].i][k].traffic_light.direction:
                    if self.road.grid[cars_right[i].i][k].traffic_light.get_color() == "red":
                        distance = abs(k - cars_right[i].j)
                        if not (distance == 1 and cars_right[i].vel >= self.v_max//2 and self.road.grid[cars_right[i].i][k].traffic_light.tick_counter == 1):
                            new_vel = min(max(distance - 1, 0), new_vel)
                        break
                    elif self.road.grid[cars_right[i].i][k].traffic_light.get_color() == "green":
                        new_vel -= 1
                        if new_vel <= 0:
                            new_vel = 1
                        break

            if i!=len(cars_right)-1:
                if cars_right[i].i == cars_right[i+1].i and (cars_right[i].j + cars_right[i].vel >= cars_right[i+1].j):
                    new_vel = min(new_vel, max(cars_right[i+1].j - cars_right[i].j - 1, 0))                
            
            if self.road.grid[cars_right[i].i][cars_right[i].j].is_crossing \
                and cars_right[i].drive_direction in self.road.grid[cars_right[i].i][cars_right[i].j].road_directions\
                and not cars_right[i].get_opposite_direction() in self.road.grid[cars_right[i].i][cars_right[i].j].road_directions:
                
                directions =  self.road.grid[cars_right[i].i][cars_right[i].j].road_directions
                pick = self.rnd.random_pick(directions)
                new_vel = min(1, new_vel)
                cars_right[i].vel = new_vel
                if pick == Direction.Direction.up:
                    cars_right[i].move()
                cars_right[i].drive_direction = pick
            cars_right[i].vel = new_vel
            cars_right[i].move()

            if cars_right[i].j >= len(self.road.grid):
                popped = cars_right.pop()
                for i in range(len(self.cars)):
                    if self.cars[i] == popped:                        
                        self.cars.pop(i)
                        break
        return self.cars

    def nagel_left(self):
        """
        Try to move cars to the left
        @return Moved cars as array
        """
        cars_left = []
        for i in self.cars:
            if i.drive_direction == Direction.Direction.left:
                cars_left.append(i)
        cars_left.sort(key=lambda x: x.j, reverse=True)

        for i in range(len(cars_left)-1,-1,-1):
            if cars_left[i].drive_direction != Direction.Direction.left:
                continue

            if self.rnd.rule_break(self.rulebreak_chance) and (cars_left[i].vel >= self.v_max and cars_left[i].vel < 10):
                cars_left[i].vel += 1
            if self.rnd.speed_up(self.speedup_chance * ((1/(cars_left[i].vel + 1)) + 1.5)) and cars_left[i].vel < self.v_max or cars_left[i].vel == 0:
                cars_left[i].vel += 1
            if self.rnd.slow_down(self.slowdown_chance) and cars_left[i].vel > 1:
                cars_left[i].vel -= 1

            new_vel = cars_left[i].vel
        
            for k in range(cars_left[i].j - 1, cars_left[i].j-cars_left[i].vel-1, -1):
                if k<0:
                    break
                if self.road.grid[cars_left[i].i][k].traffic_light != None and cars_left[i].drive_direction == self.road.grid[cars_left[i].i][k].traffic_light.direction:
                    if self.road.grid[cars_left[i].i][k].traffic_light.get_color() == "red":
                        distance = abs(k - cars_left[i].j)
                        if not (distance == 1 and cars_left[i].vel >= self.v_max//2 and self.road.grid[cars_left[i].i][k].traffic_light.tick_counter == 1):
                            new_vel = min(max(distance - 1, 0), new_vel)
                        break
                    elif self.road.grid[cars_left[i].i][k].traffic_light.get_color() == "green":
                        new_vel -= 1
                        if new_vel <= 0:
                            new_vel = 1
                        break

            if i!=len(cars_left)-1:
                if cars_left[i].i == cars_left[i+1].i and (cars_left[i].j - cars_left[i].vel <= cars_left[i+1].j):
                    new_vel = min(new_vel, max(cars_left[i].j - cars_left[i+1].j - 1, 0))                


            if self.road.grid[cars_left[i].i][cars_left[i].j].is_crossing \
                and cars_left[i].drive_direction in self.road.grid[cars_left[i].i][cars_left[i].j].road_directions\
                and not cars_left[i].get_opposite_direction() in self.road.grid[cars_left[i].i][cars_left[i].j].road_directions:
                
                directions =  self.road.grid[cars_left[i].i][cars_left[i].j].road_directions
                pick = self.rnd.random_pick(directions)
                new_vel = 1
                cars_left[i].vel = new_vel
                if pick == Direction.Direction.down:
                    cars_left[i].move()
                cars_left[i].drive_direction = pick
            cars_left[i].vel = new_vel
            cars_left[i].move()

            if cars_left[i].j < 0:
                popped = cars_left.pop()
                for i in range(len(self.cars)):
                    if self.cars[i] == popped:                        
                        self.cars.pop(i)
                        break
        return self.cars

    def nagel_up(self):
        """
        Try to move cars to the up
        @return Moved cars as array
        """
        cars_up = []
        for i in self.cars:
            if i.drive_direction == Direction.Direction.up:
                cars_up.append(i)
        cars_up.sort(key=lambda x: x.i, reverse=True)

        for i in range(len(cars_up)-1,-1,-1):
            if cars_up[i].drive_direction != Direction.Direction.up:
                continue

            if self.rnd.rule_break(self.rulebreak_chance) and (cars_up[i].vel >= self.v_max and cars_up[i].vel < 10):
                cars_up[i].vel += 1
            if self.rnd.speed_up(self.speedup_chance * ((1/(cars_up[i].vel + 1)) + 1.5)) and cars_up[i].vel < self.v_max or cars_up[i].vel == 0:
                cars_up[i].vel += 1
            if self.rnd.slow_down(self.slowdown_chance) and cars_up[i].vel > 1:
                cars_up[i].vel -= 1

            new_vel = cars_up[i].vel

            for k in range(cars_up[i].i - 1, cars_up[i].i-cars_up[i].vel-1, -1):
                if k<0:
                    break
                if self.road.grid[cars_up[i].j][k].traffic_light != None and cars_up[i].drive_direction == self.road.grid[cars_up[i].j][k].traffic_light.direction:
                    if self.road.grid[cars_up[i].j][k].traffic_light.get_color() == "red":
                        distance = abs(k - cars_up[i].i)
                        if not (distance == 1 and cars_up[i].vel >= self.v_max//2 and self.road.grid[cars_up[i].j][k].traffic_light.tick_counter == 1):
                            new_vel = min(max(distance - 1, 0), new_vel)
                        break
                    elif self.road.grid[cars_up[i].j][k].traffic_light.get_color() == "green":
                        new_vel -= 1
                        if new_vel <= 0:
                            new_vel = 1
                        break
            
            if i!=len(cars_up)-1:
                if cars_up[i].j == cars_up[i+1].j and (cars_up[i].i - cars_up[i].vel <= cars_up[i+1].i):
                    new_vel = min(new_vel, max(cars_up[i].i - cars_up[i+1].i - 1, 0))                

            if self.road.grid[cars_up[i].j][cars_up[i].i].is_crossing \
                and cars_up[i].drive_direction in self.road.grid[cars_up[i].j][cars_up[i].i].road_directions\
                and not cars_up[i].get_opposite_direction() in self.road.grid[cars_up[i].j][cars_up[i].i].road_directions:
                directions =  self.road.grid[cars_up[i].j][cars_up[i].i].road_directions
                pick = self.rnd.random_pick(directions)
                new_vel = 1
                cars_up[i].vel = new_vel
                if pick == Direction.Direction.left:
                    cars_up[i].move()
                cars_up[i].drive_direction = pick

            cars_up[i].vel = new_vel
            cars_up[i].move()

            if cars_up[i].i < 0:
                popped = cars_up.pop()
                for i in range(len(self.cars)):
                    if self.cars[i] == popped:                        
                        self.cars.pop(i)
                        break
        return self.cars

    def nagel_down(self):
        """
        Try to move cars to the down
        @return Moved cars as array
        """

        # Create vector of cars driving down
        cars_down = []
        for i in self.cars:
            if i.drive_direction == Direction.Direction.down:
                cars_down.append(i)
        cars_down.sort(key=lambda x: x.i)
        # Iterate over all car
        for i in range(len(cars_down)-1,-1,-1):
            # If direction != down continue
            if cars_down[i].drive_direction != Direction.Direction.down:
                continue


            # Random events
            if self.rnd.rule_break(self.rulebreak_chance) and (cars_down[i].vel >= self.v_max and cars_down[i].vel < 10):
                cars_down[i].vel += 1
            if self.rnd.speed_up(self.speedup_chance * ((1/(cars_down[i].vel + 1)) + 1.5)) and cars_down[i].vel < self.v_max or cars_down[i].vel == 0:
                cars_down[i].vel += 1
            if self.rnd.slow_down(self.slowdown_chance) and cars_down[i].vel > 1:
                cars_down[i].vel -= 1

            new_vel = cars_down[i].vel

            # Check for traffic light
            for k in range(cars_down[i].i + 1, cars_down[i].i+cars_down[i].vel+1):
                if k>=len(self.road.grid[i]):
                    break
                if self.road.grid[cars_down[i].j][k].traffic_light != None and cars_down[i].drive_direction == self.road.grid[cars_down[i].j][k].traffic_light.direction:
                    if self.road.grid[cars_down[i].j][k].traffic_light.get_color() == "red":
                        distance = abs(k - cars_down[i].i)
                        if not (distance == 1 and cars_down[i].vel >= self.v_max//2 and self.road.grid[cars_down[i].j][k].traffic_light.tick_counter == 1):
                            new_vel = min(max(distance - 1, 0), new_vel)
                        break
                    elif self.road.grid[cars_down[i].j][k].traffic_light.get_color() == "green":
                        new_vel -= 1
                        if new_vel <= 0:
                            new_vel = 1
                        break

            if i!=len(cars_down)-1:
                if cars_down[i].j == cars_down[i+1].j and (cars_down[i].i + cars_down[i].vel >= cars_down[i+1].i):
                    new_vel = min(new_vel, max(cars_down[i+1].i - cars_down[i].i - 1, 0))                
            
            if self.road.grid[cars_down[i].j][cars_down[i].i].is_crossing \
                and cars_down[i].drive_direction in self.road.grid[cars_down[i].j][cars_down[i].i].road_directions\
                and not cars_down[i].get_opposite_direction() in self.road.grid[cars_down[i].j][cars_down[i].i].road_directions:
                
                directions =  self.road.grid[cars_down[i].j][cars_down[i].i].road_directions
                pick = self.rnd.random_pick(directions)
                new_vel = min(1, new_vel)
                cars_down[i].vel = new_vel
                if pick == Direction.Direction.right:
                    cars_down[i].move()
                cars_down[i].drive_direction = pick
            cars_down[i].vel = new_vel
            cars_down[i].move()

            if cars_down[i].i >= len(self.road.grid):
                popped = cars_down.pop()
                for i in range(len(self.cars)):
                    if self.cars[i] == popped:                        
                        self.cars.pop(i)
                        break
        return self.cars
