import pygame
import math


def draw(screen, car, road_map_array, font):
    """
    Draw all cars on map (This function doesn't draw map).
    @param screen PyGame display screen class
    @param car List of all cars in map
    @param road_map_array Class of map
    @param font Font type
    """

    # Map width
    y = len(road_map_array)

    # Mapa musi miec dwa wymiary
    assert y != 0, "Map must have 2 dimension"

    # Map height
    x = len(road_map_array[0])

    # Screen resolution
    screen_size = screen.get_size()

    # Box X size (tile size)
    box_x_size = (screen_size[0] / x)
    # Box Y size (tile size)
    box_y_size = (screen_size[1] / y)

    # Car X size
    car_x_size = (screen_size[0] / x)
    # Car Y size
    car_y_size = (screen_size[1] / y)

    # Car postion on map (x, i)
    car_x = car.j * box_x_size
    # Car postion on map (y, j)
    car_y = car.i * box_y_size
    
    # Color of car
    car_color = car.color
    
    # Kierunek jazdy samochodu (góra, dół, lewo, prawo)

    # Car object
    car_obj = pygame.Rect(car_x, car_y , box_x_size, box_y_size)
    
    # Draw car
    pygame.draw.rect(screen, car_color, car_obj)

    # Speed value text
    text = font.render(str(car), True, (0, 0, 0))

    # Text rectangle    
    textRect = text.get_rect()

    # Text offset
    offset = font.get_height() / 2
    textRect.center = (car_x + offset * 1.3, car_y + offset)
    
    screen.blit(text, textRect)

    # Draw white frame around car
    pygame.draw.rect(screen, (255, 255, 255), car_obj, int((car_x_size * 4) / 28))



