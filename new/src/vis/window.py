import pygame

import eng.Motion as Motion
import vis.configure as configure
import vis.draw_road as draw_road
import vis.draw_car as draw_car
import vis.traffic_light as Traffic
import eng.Road as Road
import time

def game_loop():
    """
    Main game loop.
    Configure program: 
    Screen resolution, FPS, map size,
    initialise pygame,
    Move every car etc.
    """

    loop = 0

    # Initialise motion class
    motion = Motion.Motion()

    # Game fps
    FPS = 1
    # PyGame fps clock
    fpsClock = pygame.time.Clock()

    # Configuration (screen resolution)
    config = configure.config()

    # Map
    road = motion.road

    # Number of cells (height)
    y = len(road.grid)

    # Number of cells (width)
    x = len(road.grid[0])

    # Box X size
    box_x_size = (config.resolution[0] / x)
    # Box Y size
    box_y_size = (config.resolution[1] / y)

    # Traffic light array
    traffic = Traffic.generate_traffic_light_array(road.grid)


    pygame.init()

    # PyGame screen
    screen = pygame.display.set_mode(config.resolution)
    
    # Window name
    pygame.display.set_caption('IPZ')

    # Font
    font = pygame.font.Font('freesansbold.ttf', 12)
    running = True
    while running:
        # Random chance to spawn new car on map
        motion.spawn()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        screen.fill((255, 255, 255))
    
        draw_road.draw(screen, road.grid, font)
        
        #Insert tick here vvvv
        #Traffic light tick
        for i in range(len(traffic)):
            traffic[i].tick()
        loop = loop + 1
        if loop % 5 == 0:
            loop = 0
            f = open('samochody.txt', "a")
            f.write(str(len(motion.cars)) + '\n')
            f.close()
        # motion.cars array of all cars
        # Draw cars on map
        for car in motion.cars:
            draw_car.draw(screen, car, road.grid, font)

        # Draw traffic light
        Traffic.draw_traffic(screen, traffic)

        # Tick every traffic light
        for i in range(len(traffic)):
            traffic[i].tick()
        
        # Move every car in map
        motion.nagel_right()
        motion.nagel_left()
        motion.nagel_up()
        motion.nagel_down()

        #Uncomment this if you want to show road with cars in console
        #motion.road.show(motion.cars)
        #print(motion.cars)
        pygame.display.flip()
        fpsClock.tick(FPS)
    f.close()
    pygame.quit()


