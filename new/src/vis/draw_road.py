import pygame
import vis.translate_tiles as tiles
import eng.Direction as Direction
import math


def draw(screen, road_map_array, font):
    """
    Draw map
    @param screen PyGame display screen class
    @param road_map_array Class of map
    @param font Font type
    """
    # Map width
    y = len(road_map_array)

    assert y != 0, "Map must have 2 dimension."

    # Map height
    x = len(road_map_array[0])

    ## Screen resolution
    screen_size = screen.get_size()

    # Box X size
    box_x_size = (screen_size[0] / x)
    # Box Y size
    box_y_size = (screen_size[1] / y)

    # Draw all tiles on map
    for i in range(x):
        for j in range(y):
            # Color of tiles
            tiles_color = tiles.translate(road_map_array[j][i])

            # X tile postion
            tile_x = i * box_x_size
            # Y tile postion
            tile_y = j * box_y_size

            # Tile object
            rectangle = pygame.Rect(tile_x, tile_y , math.ceil(box_x_size), math.ceil(box_y_size))
            
            # Draw tile
            pygame.draw.rect(screen, tiles_color, rectangle)

            # Create text that show direction on road
            if road_map_array[j][i].tile_type == "street" and len(road_map_array[j][i].road_directions) == 1 and not road_map_array[j][i].is_obstacle: 
                direct = road_map_array[j][i].road_directions[0]
                ch = '<'
                if direct == Direction.Direction.right:
                    ch = '>'
                elif direct == Direction.Direction.up:
                    ch = '^'
                elif direct == Direction.Direction.down:
                    ch = 'v'

                text = font.render(ch, True, (255,255,255))
                
                textRect = text.get_rect()

                offset = font.get_height() / 2
                textRect.center = (tile_x + offset, tile_y + offset)
                
                screen.blit(text, textRect)


