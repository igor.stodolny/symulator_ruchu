
def translate(road_block):
    """
    Translate road type to RGB color
    Street -> Black
    Grass -> Green
    etc ...
    @param road_block One block of road 
    @return RGB color of tiles
    """
    if road_block.traffic_light != None:
        return (0, 0, 200)
    if road_block.is_obstacle:
        return (200, 0, 0)
    if road_block.tile_type == "street":
        return (0, 0, 0)

    if road_block.tile_type == "grass":
        return (0, 200, 0)

    print("{} - unknown road tile type.".format(road_block.tile_type))
    raise "Bad road tile type"
        
