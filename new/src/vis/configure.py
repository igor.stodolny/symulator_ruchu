class config:
    """ Config screen class
    """
    def __init__(self, resolution = [600, 600]):
        """Initialise screen resolution
        @param resolution
        Screen resolution
        """
        ## @var resolution
        #  Resolution of screen
        self.resolution = resolution
