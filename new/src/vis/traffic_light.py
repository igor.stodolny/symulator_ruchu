
import pygame
import os


def draw_traffic(screen, traffic_array):
    """
    Draw traffic light
    @param screen PyGame display screen class
    @param traffic_array Array of classes of TrafficLight
    """
    for light in traffic_array:
        pos_x = light.traffic.x_vis
        pos_y = light.traffic.y_vis
        screen.blit(light.get_light(), (pos_x, pos_y))

def generate_traffic_light_array(road):
    """
    Generates array of traffic light, that are in our map.
    @param road Our map with traffic light
    @return array of traffic lights 
    """
    traffic = []
    for i in range(len(road)):
        for j in range(len(road[i])):
            if road[i][j].traffic_light != None:
                traffic.append(traffic_light(30, 80, road[i][j].traffic_light ))
    return traffic

class traffic_light:
    """Visual traffic_light class.
    It containts traffic class from eng/TrafficLight.py
    Class is responsible to display traffic light on screen.
    """

    ## @var red_light
    # Red light texture
    red_light = pygame.image.load(os.path.join('..', 'texture', 'traffic_light', 'red_light.png'))
    
    ## @var green_light
    # Green light texture
    green_light = pygame.image.load(os.path.join('..', 'texture', 'traffic_light', 'green_light.png'))

    def __init__(self, width, height, traffic):
        """ Visual traffic_light constructor.
        @param width Width of traffic light
        @param height Height of traffic light
        @param traffic Traffic light class (eng/TrafficLight.py)
        """
        ## @var width
        # Width of traffic light 
        self.width = width

        ## @var height
        # Height of traffic light 
        self.height = height

        ## @var traffic
        # Traffic light class
        self.traffic = traffic

    def tick(self):
        """ One tick
        """
        self.traffic.tick()

    def set_light(self, new_light):
        """ Change color of light
         @param new_light New light must be "red" or "green"
        """
        assert new_light == "red" or new_light == "green", "traffic light color can be 'red' or 'green'"
        self.traffic.color = new_light
    
    def get_light(self):
        """ Get current light as pygame object (ready to draw)
        @return Light pygame object
        """
        orginal = traffic_light.red_light
        if self.traffic.get_color() == "green":
            orginal = traffic_light.green_light
        light = orginal
        light = pygame.transform.scale(orginal, (self.width, self.height))
        return light