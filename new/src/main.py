import vis.window as window

if __name__ == "__main__":
    """ Main function -> Call game loop
    """
    window.game_loop()
