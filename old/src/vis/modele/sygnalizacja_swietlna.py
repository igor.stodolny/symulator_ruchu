import math
import pygame

from src.eng.road import road_type as road_type
import src.eng.modele.sygnalizacja_swietlna as ss
import src.vis.traffic_light as tl

# Lenght of road
road_len = 30

# Cars vmax
v_max = 5

# Green light is turn on for X ticks
green_tick = 10
# Red light is turn on for X ticks
red_tick = 5
# Traffic light model
SygnalizacjaSwietlna = ss.SygnalizacjaSwietlna(road_len, v_max, green_tick, red_tick)
# traffic light width
tr_width = 100
#traffic light height
tr_height = 200

#Traffic light class
traffic_light = tl.traffic_light(0, 0, tr_width , tr_height)

# Map to draw
temporary_map = []

#Generate started (empty) map
def ss_generate_map():
    for i in range(road_len):
        row = []
        #If its middle of map draw road
        if i == math.ceil(road_len/2) - 1:
            row = [road_type.street for j in range(road_len)]
        else:
        # In otherwise draw green
            row = [road_type.grass for j in range(road_len)]
            # Draw gray pole in place where traffic light are
            if i <= math.ceil(road_len/2) - 2 and i >= math.ceil(road_len/2) - 5:
                row[SygnalizacjaSwietlna.traffic_light] = road_type.gray_pole
        # Append one row to map
        temporary_map.append(row)
    return temporary_map

def ss_tick(screen):
    # Simulate model (1 tick)
    cars = SygnalizacjaSwietlna.tick()
    # set light texture
    light = road_type.red_light

    # If light green is turn on change traffic light to green
    if SygnalizacjaSwietlna.traffic_light_green:
        light = road_type.green_light
    
    # Set traffic light
    traffic_light.set_light(light)
    
    #Calculate traffic light x position
    tr_x = pygame.display.Info().current_w # Width screen
    traffic_pos = SygnalizacjaSwietlna.traffic_light/road_len # traffic light position on map divide by road length
    tr_x = tr_x * traffic_pos # Point to traffic light on map
    tr_x = tr_x - tr_width/2 # Center traffic light
    tr_x = tr_x + (pygame.display.Info().current_w/road_len)/2 # Add box width
    #Calculate traffic light y position
    tr_y = pygame.display.Info().current_h # Height screen
    road_pos = (math.ceil(road_len/2) - 1 - 4) / road_len # Road position on screen divide by road length
    tr_y = tr_y * road_pos # Middle of screen
    tr_y = tr_y - tr_height # Center traffic light
    #Set traffic light position
    traffic_light.set_position((tr_x, tr_y))
    #Draw traffic light
    screen.blit(traffic_light.get_light(), traffic_light.get_position())
    #calulace vis position of cars
    for car in cars:
        # Change visual position of car
       car.vis_i = math.ceil(road_len / 2) - 1 
       car.vis_j = car.j
    return cars