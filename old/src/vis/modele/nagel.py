import src.eng.modele.nagel as nagel
import math

from src.eng.road import road_type as road_type

def nagel_generate_map():
    temporary_map = []
    for i in range(nagel.road_len):
        row = []
        if i == math.ceil(nagel.road_len/2) - 1:
            row = [road_type.street for j in range(nagel.road_len)]
        else:
            row = [road_type.grass for j in range(nagel.road_len)]
        temporary_map.append(row)
    return temporary_map


def nagel_tick():
    cars = nagel.nagel(nagel.cars, nagel.road_len)
    for car in cars:
       car.vis_i = math.ceil(nagel.road_len/2) - 1
       car.vis_j = car.j
    return cars
