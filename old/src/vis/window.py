import pygame

import src.vis.configure as configure
import src.vis.draw_road as draw_road
import src.vis.draw_car as draw_car

# Modele
import src.vis.modele.nagel as nagel
import src.vis.modele.sygnalizacja_swietlna as sygnalizacja_swietlna

import time

# engine
import src.eng.car as car


from enum import Enum
class model(Enum):
    nagel = 0
    sygnalizacja_swietlna = 1

def game_loop(SimulationModel):

    # Game fps
    FPS = 2
    fpsClock = pygame.time.Clock()

    # List of cars on map
    Cars = []

    # Map
    grid = 0

    # Pick simulation model
    if SimulationModel == model.nagel:
        grid = nagel.nagel_generate_map()
    elif SimulationModel == model.sygnalizacja_swietlna:
        grid = sygnalizacja_swietlna.ss_generate_map()

    # Load config (screen resolution)
    config = configure.config()

    pygame.init()

    # Change screen resolution
    screen = pygame.display.set_mode(config.resolution)
    
    # Window name
    pygame.display.set_caption('IPZ')

    running = True
    while running:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        screen.fill((255, 255, 255))
    
        draw_road.draw(screen, grid)
        
        # Check simulation model
        if SimulationModel == model.nagel:
            Cars = nagel.nagel_tick()
        elif SimulationModel == model.sygnalizacja_swietlna:
            Cars = sygnalizacja_swietlna.ss_tick(screen)
        
        # Draw cars on map
        for car in Cars:
            draw_car.draw(screen, car, grid)

        pygame.display.flip()
        fpsClock.tick(FPS)

    pygame.quit()


