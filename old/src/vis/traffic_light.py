from src.eng.road import road_type as road_type

import pygame
import os

# Traffic light class
class traffic_light:
    # Red light texture
    red_light = pygame.image.load(os.path.join('texture', 'traffic_light', 'red_light.png'))
    # Green light texture
    green_light = pygame.image.load(os.path.join('texture', 'traffic_light', 'green_light.png'))
  
    def __init__(self, x, y, width, height, start_light = road_type.red_light):
        assert start_light == road_type.red_light or start_light == road_type.green_light, "start light can be only green or red"
        # x positon
        self.x = x
        # y position
        self.y = y
        # width of traffic light
        self.width = width
        # height of traffic light
        self.height = height
        # Actual light (green or red)
        self.light = start_light
    
    # Change position of traffic light
    def set_position(self, position):
        self.x = position[0]
        self.y = position[1]
        
    # Get position of traffic light
    def get_position(self):
        return (self.x, self.y)

    # Change traffic ligh color
    def set_light(self, new_light):
        assert new_light == road_type.red_light or new_light == road_type.green_light, "light can be only green or red"
        self.light = new_light
    
    # Get current light as pygame object (drawable)
    def get_light(self):
        orginal = traffic_light.red_light
        if self.light == road_type.green_light:
            orginal = traffic_light.green_light
        light = orginal
        light = pygame.transform.scale(orginal, (self.width, self.height))
        return light