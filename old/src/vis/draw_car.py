import pygame
import math

from src.eng.car import direction

def draw(screen, car, road_map_array):
    # Map width
    y = len(road_map_array)

    # Mapa musi miec dwa wymiary
    assert y != 0, "Map must have 2 dimension"

    # Map height
    x = len(road_map_array[0])

    # Screen resolution
    screen_size = screen.get_size()

    # Box size
    box_x_size = (screen_size[0] / x)
    box_y_size = (screen_size[1] / y)

    # Car size
    car_x_size = (screen_size[0] / x)
    car_y_size = (screen_size[1] / y)

    # Car postion on map
    car_x = car.vis_j
    car_y = car.vis_i
    
    # Set car color
    car_color = car.color
    
    # Kierunek jazdy samochodu (góra, dół, lewo, prawo)

    # Car object
    car_obj = pygame.Rect(car.vis_j * box_x_size, car.vis_i * box_y_size , box_x_size, box_y_size)
    
    # Draw car
    pygame.draw.rect(screen, car_color, car_obj)

    # Draw white frame around car
    pygame.draw.rect(screen, (255, 255, 255), car_obj, int((car_x_size * 4) / 28))



