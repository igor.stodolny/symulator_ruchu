from src.eng.road import road_type as road_type
import pygame

# Translate road type to RGB color
def translate(road):
    if road == road_type.grass:
        return (0, 255, 0) # Green
    if road == road_type.street:
        return (0, 0, 0) # Black
    if road == road_type.green_light:
        return (0, 200, 0) # Light green
    if road == road_type.red_light:
        return (255, 0, 0) # Red
    if road == road_type.black_tiles:
        return (0, 0, 0) # Black
    if road == road_type.white_frame:
        return (255, 255, 255) # White
    if road == road_type.gray_pole:
        return (220, 220, 220) # Gray