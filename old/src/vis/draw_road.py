import pygame
import src.vis.translate_tiles as tiles
import math
def draw(screen, road_map_array):
    # Map width
    y = len(road_map_array)

    assert y != 0, "Map must have 2 dimension."

    # Map height
    x = len(road_map_array[0])

    # Screen resolution
    screen_size = screen.get_size()

    # Box size
    box_x_size = (screen_size[0] / x)
    box_y_size = (screen_size[1] / y)

    # Draw all tiles on map
    for i in range(x):
        for j in range(y):
            # Color of tiles
            tiles_color = tiles.translate(road_map_array[j][i])

            # Tile object
            rectangle = pygame.Rect(i * box_x_size, j * box_y_size , math.ceil(box_x_size), math.ceil(box_y_size))
            
            # Draw tile
            pygame.draw.rect(screen, tiles_color, rectangle)


