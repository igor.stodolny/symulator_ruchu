import random

from enum import Enum
class direction(Enum):
    up = 8
    down = 2
    right = 6
    left = 4


cars_colors = [(255, 0, 255), (0, 255, 255), (255,255,0), (255,250,205), (244, 164, 96),(135, 206, 250)]
last_color = 0
def gen_color():
    global last_color
    new_color = cars_colors[random.randint(0, len(cars_colors) - 1)]
    while new_color == last_color:
        new_color = cars_colors[random.randint(0, len(cars_colors) - 1)]
    last_color = new_color
    return new_color

class Car:
    exit_positions = [(0, 10)]

    # Static
    def __init__(self, i, j, drive_direction ,destination, vel = 0):
        assert destination < len(Car.exit_positions), "Destination out of bounds"
        assert isinstance(vel, int), "Velocity must be an int"
        assert isinstance(i, int), "i must be an int"
        assert isinstance(j, int), "j must be an int"
        assert isinstance(drive_direction, direction) , "Direction must be an class enum direction"


        self.color: int = gen_color()
        self.i: int = i
        self.j: int = j

        self.vis_i = self.i
        self.vis_j = self.j
        
        self.destination = destination
        self.vel = vel
        self.length = 4
        self.drive_direction = drive_direction

    def move(self):
        if self.drive_direction == direction.right:
            self.j += self.vel
        elif self.drive_direction == direction.left:
            self.j -= self.vel
        elif self.drive_direction == direction.up:
            self.i -= self.vel
        elif self.drive_direction == direction.down:
            self.i += self.vel
    

    def __repr__(self):
        return str(self.vel)

    def get_velocity(self):
        return self.vel
    
    def set_velocity(self, new_velocity):
        assert isinstance(new_velocity, int), "Velocity must be an int"
        self.vel = new_velocity

    def get_direction(self):
        return self.drive_direction
    
    def set_direction(self, new_direction):
        assert isinstance(new_direction, direction) , "Direction must be an class enum direction"
        self.drive_direction = new_direction