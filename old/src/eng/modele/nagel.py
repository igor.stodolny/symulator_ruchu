from random import randrange
import time

from src.eng.car import Car, direction

v_max = 4 # 10->144km/h, 9->130km/h, 8->115km/h, 7->101km/h, 6->86km/h,
          # 5->72km/h,   4->57km/h,  3->43km/h,  2->29km/h,  1->14km/h
showup_chance = 4 #1->100%, 2->50%, 4->25% 10->10%
slowdown_chance = 40 #1->100%, 2->50%, 4->25% 10->10%
speedup_chance = 4 #1->100%, 2->50%, 4->25% 10->10%
rulebreak_chance = 10 #1->100%, 2->50%, 4->25% 10->10%

road_len = 51
cars = []
road = [' ' for i in range(road_len)]


def convTable(tab):
    res='['
    for i in tab:
        res+=str(i)
    res+=']'
    return res

def printCarsOnRoad(cars):
    res = [' ' for i in range(road_len)]
    for i in cars:
        res[i.j]=str(i.vel)
    return res

def nagel(cars, road_len):
    for i in range(len(cars)-1,-1,-1):
        decider_speedup = randrange(speedup_chance)
        decider_slowdown = randrange(slowdown_chance)
        decider_rulebreak = randrange(rulebreak_chance)

        if decider_rulebreak in [0] and (cars[i].vel >= v_max and cars[i].vel < 10):
            cars[i].vel += 1
        if decider_speedup in [0] and cars[i].vel < v_max:
            cars[i].vel += 1
        if decider_slowdown in [0] and cars[i].vel > 1:
            cars[i].vel -= 1

        if cars[i].j + cars[i].vel < road_len:
            if i!=len(cars)-1:
                if cars[i].j + cars[i].vel < cars[i+1].j:
                    cars[i].j += cars[i].vel
                else:
                    cars[i].vel = max(cars[i+1].j - cars[i].j - 1, 0)
                    cars[i].j += cars[i].vel
            else:
                cars[i].j+=cars[i].vel
        else:
            cars.pop()

    decider_showup = randrange(showup_chance)

    if decider_showup in [0]:
        cars.insert(0, Car(i=0, j=0, drive_direction=direction.right, destination=0, vel=randrange(1, v_max+1)))

    return cars


def nagel_print_console():
    #print(convTable(printCarsOnRoad(nagel(cars, road_len))))
    while True:
        print("\r", convTable(printCarsOnRoad(nagel(cars, road_len))), end="", flush=True)
        time.sleep(1)
    #input('')
