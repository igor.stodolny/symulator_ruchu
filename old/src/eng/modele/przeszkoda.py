from random import randrange
import random
import time
#from src.eng.car import Car, direction
import numpy as np
from enum import Enum
N=21
class direction(Enum):
    up = 8
    down = 2
    right = 6
    left = 4

cars = []
showup_chance = 4
v_max = 4


class Obstacle:
    def __init__(self):
        self.przeszkoda = np.random.randint(N/3, N/2)
        self.przeszkoda2 = np.random.randint(N/2+1,N)
    def get_coords(self):
        return self.przeszkoda,self.przeszkoda2

class Board:
    def __init__(self):
        obsC = Obstacle()
        self.board = np.ones((2, N), dtype='int')
        self.obs_i, self.obs_j = obsC.get_coords()
        self.flag1 = False
        self.flag2 = False

    def checkFirstCell(self):
        self.flag1 = self.board[0, 0] == 1
        self.flag2 = self.board[1, 0] == 1
        return self.flag1,self.flag2

    def gen_obs(self, obs_i, obs_j):
        self.board[obs_i, obs_j] = 2
BOARD = Board()



def tick():
    decider_showup = randrange(showup_chance)
    obsC = Obstacle()


    flaga1,flaga2 = BOARD.checkFirstCell()

    if 0.7 > random.random() and flaga1:
        cars.insert(0, Car(i=0, j=0, drive_direction=direction.right, destination=0, vel=randrange(1, v_max + 1)))
    if 0.7 > random.random() and flaga2:
        cars.insert(0, Car(i=1, j=0, drive_direction=direction.right, destination=0, vel=randrange(1, v_max + 1)))
    for car in cars:
        b = przeszkoda2J-car.j
        BOARD.board[car.i][car.j] = 3
        if car.vel < 5:
            car.vel = 0
            car.j += car.vel
        else:
            car.j+=car.vel
        if car.j + car.vel <= b-1: # ??? jak to zrobic
            if car.i == 1:
                car.i -= 1
            else:
                car.i += 1

        if car.j+car.vel > N:
            cars.pop()



class Car:
    exit_positions = [(0, 10)]
    start_positions = [(0, 5)]

    # Static
    def __init__(self, i, j, drive_direction, destination, vel=0):
        assert destination < len(Car.exit_positions), "Destination out of bounds"
        assert isinstance(vel, int), "Velocity must be an int"
        assert isinstance(i, int), "i must be an int"
        assert isinstance(j, int), "j must be an int"
        assert isinstance(drive_direction, direction), "Direction must be an class enum direction"
        obstacle = Obstacle()
        self.i: int = i
        self.j: int = j

        self.vis_i = self.i
        self.vis_j = self.j

        self.destination = destination
        self.vel: float = vel
        self.length = 4
        self.drive_direction = drive_direction

        def move(self):
            if self.drive_direction == direction.right:
                self.j += self.vel
            elif self.drive_direction == direction.left:
                self.j -= self.vel
            elif self.drive_direction == direction.up:
                self.i -= self.vel
            elif self.drive_direction == direction.down:
                self.i += self.vel



        def __repr__(self):
            # return str(self.vel)
            return f"{self.i},{self.j}"

        def get_velocity(self):
            return self.vel

        def set_velocity(self, new_velocity):
            assert isinstance(new_velocity, int), "Velocity must be an int"
            self.vel = new_velocity

        def get_direction(self):
            return self.drive_direction

        def set_direction(self, new_direction):
            assert isinstance(new_direction, direction), "Direction must be an class enum direction"
            self.drive_direction = new_direction

        def getObstacle(self):
            koordy = Obstacle()
            i,j=koordy.get_coords()
            return i,j



        #do dokonczenia, zastąpić goRight
        # def goRight2(self,cars,board):
        #     board = Board()
        #     przeszkodaI, przeszkodaJ = getObstacle()
        #     for i in range(N):
        #         if (cars[i].drive_direction == direction.right):
        #             if cars[i].j + cars[i].vel < len(board.board[0]):
        #                 if cars[i].j + cars[i].vel >= board[przeszkodaI][przeszkodaJ]:
        #                     cars[i].vel = 0





#def goRight(cars,board):



if __name__ == "__main__":
    OBS = Obstacle()
    przeszkoda1J,przeszkoda2J=OBS.get_coords()
    # print(BOARD.board)
    BOARD.gen_obs(0,przeszkoda1J)
    BOARD.gen_obs(1,przeszkoda2J)

    while True:
        tick()
        time.sleep(1)
        print(BOARD.board)