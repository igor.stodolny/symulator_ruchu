import random
import time
#from src.eng.car import Car, direction
import numpy as np
from enum import Enum

def gen_color():
    r = random.randint(50, 255)
    g = 0
    b = random.randint(50, 255)
    return (r, g, b)

class direction(Enum):
    up = 8
    down = 2
    right = 6
    left = 4



N = 31
CARS_VERTICAL = []
CARS_HORIZONTAL = []
showup_chance = 4
v_max = 4


showup_chance = 4 #1->100%, 2->50%, 4->25% 10->10%
slowdown_chance = 40 #1->100%, 2->50%, 4->25% 10->10%
speedup_chance = 4 #1->100%, 2->50%, 4->25% 10->10%
rulebreak_chance = 10 #1->100%, 2->50%, 4->25% 10->10%


class Spot:
    def __init__(self, category):
        self.category = category
        self.v_max = 0
    def __repr__(self):
        return str(self.category)

class TLigts:
    def __init__(self, vertical = 1, horizontal = 0):
        self.vertical = vertical #0-czerwone, 1-zielone
        self.horizontal = horizontal #0-czerwone, 1-zielone

class Board:
    def __init__(self, N):
        if N % 2 == 0:
            N += 1
        self.BOARD = [[Spot(0) for j in range(N)] for i in range(N)]
        for i, row in enumerate(self.BOARD):
            for j, col in enumerate(row):
                if(i == len(self.BOARD)//2):
                    for elem in row:
                        elem.category = 1
                        elem.v_max = 7
                if(j == len(self.BOARD[0])//2):
                    self.BOARD[i][j].category = 1
                    self.BOARD[i][j].v_max = 7
        
        self.BOARD[N//2][N//2].v_max = 8
    def __repr__(self):
        res = ""
        for i in self.BOARD:
            res += str(i) + '\n'
        return res

    def update(self, car):
        self.BOARD[car.i][car.j] = 3
    
class Car:
    exit_positions = [(0, 10)]
    start_positions = [(0, 5)]

    # Static
    def __init__(self, i, j, drive_direction ,destination, vel = 0):
        assert destination < len(Car.exit_positions), "Destination out of bounds"
        assert isinstance(vel, int), "Velocity must be an int"
        assert isinstance(i, int), "i must be an int"
        assert isinstance(j, int), "j must be an int"
        assert isinstance(drive_direction, direction) , "Direction must be an class enum direction"


        self.color: int = gen_color()
        self.i: int = i
        self.j: int = j

        self.vis_i = self.i
        self.vis_j = self.j
        
        self.destination = destination
        self.vel: float = vel
        self.length = 4
        self.drive_direction = drive_direction

    def move(self):
        if self.drive_direction == direction.right:
            self.j += self.vel
        elif self.drive_direction == direction.left:
            self.j -= self.vel
        elif self.drive_direction == direction.up:
            self.i -= self.vel
        elif self.drive_direction == direction.down:
            self.i += self.vel
    

    def __repr__(self):
        #return str(self.vel)
        return f"{self.i},{self.j}"

    def get_velocity(self):
        return self.vel
    
    def set_velocity(self, new_velocity):
        assert isinstance(new_velocity, int), "Velocity must be an int"
        self.vel = new_velocity

    def get_direction(self):
        return self.drive_direction
    
    def set_direction(self, new_direction):
        assert isinstance(new_direction, direction) , "Direction must be an class enum direction"
        self.drive_direction = new_direction

def chowd(cars, board, lights, flag):
    for i in range(len(cars)-1,-1,-1):
        decider_speedup = random.randrange(speedup_chance)
        decider_slowdown = random.randrange(slowdown_chance)
        decider_rulebreak = random.randrange(rulebreak_chance)

        if decider_rulebreak in [0] and (cars[i].vel >= v_max and cars[i].vel < 10):
            cars[i].vel += 1
        if decider_speedup in [0] and cars[i].vel < v_max:
            cars[i].vel += 1
        if decider_slowdown in [0] and cars[i].vel > 1:
            cars[i].vel -= 1


        if flag == 1:
            if cars[i].j + cars[i].vel + 1 <= len(board.BOARD[0]) - 1:
                if i!=len(cars)-1:
                    cars[i].vel += 1
                    d = cars[i+1].j - cars[i].j 
                    s = abs(cars[i].j - N//2) 
                    if lights.vertical == 0 and min(d, s) <= cars[i].vel:
                        cars[i].vel = min(d, s) - 1
                        cars[i].j += cars[i].vel
                    elif lights.vertical == 1:
                        if d < s:
                            cars[i].vel = d - 1
                            cars[i].j += cars[i].vel
                        else:
                            cars[i].vel = min(cars[i].vel, d-1)
                            cars[i].j += cars[i].vel
                    else:
                        cars[i].j += cars[i].vel
                else:
                    s = abs(cars[i].j - N//2)
                    if lights.vertical == 0 and cars[i].vel >= s:
                        cars[i].vel = s - 1
                        cars[i].j += cars[i].vel
                    elif lights.vertical == 0 and cars[i].j == N//2 -1 :
                        cars[i].vel = 0
                    else:
                        cars[i].vel += 1
                        cars[i].j += cars[i].vel
            else:
                cars.pop()
        else:
            if cars[i].i - cars[i].vel >= 0:
                if i!=len(cars)-1:
                    cars[i].vel += 1
                    d = abs(cars[i].i - cars[i+1].i)
                    s = abs(cars[i].i - N//2)
                    if lights.horizontal == 0 and min(d, s) <= cars[i].vel:
                        cars[i].vel = min(d, s) - 1
                        cars[i].i -= cars[i].vel
                    elif lights.horizontal == 1:
                        if d < s:
                            cars[i].vel = d - 1
                            cars[i].i -= cars[i].vel
                        else:
                            cars[i].vel = min(cars[i].vel, d-1)
                            cars[i].i -= cars[i].vel
                    else:
                        cars[i].i -= cars[i].vel
                else:
                    s = abs(cars[i].i - N//2)
                    if lights.horizontal == 0 and cars[i].vel >= s:
                        cars[i].vel = s - 1
                        cars[i].i -= cars[i].vel
                    elif lights.horizontal == 0 and cars[i].i == N//2 + 1 :
                        cars[i].vel = 0
                    else:
                        cars[i].vel += 1
                        cars[i].i -= cars[i].vel
                    
            else:
                cars.pop()

    if random.random() < 0.25 or len(cars) == 0:
        if flag == 1:
            cars.insert(0, Car(i=15, j=0, drive_direction=direction.right, destination=0, vel=1))
        else:
            cars.insert(0, Car(i=29, j=15, drive_direction=direction.up, destination=0, vel=1))
    board.update(cars[0])
    return cars





def main():
    BOARD = Board(N)
    counter = 0
    lights = TLigts()
    flag = 0
    next_green = 'vertical'
    while True:
        
        
        for car in CARS_VERTICAL:
            BOARD.update(car)
        
        for car in CARS_HORIZONTAL:
            BOARD.update(car)
        print(BOARD)
        if flag == 1:
            if next_green == 'vertical':
                lights.vertical = 1
                flag = 0
            else:
                lights.horizontal = 1
                flag = 0
        else:
            if random.random() > 0.5:
                if lights.vertical == 1:
                    lights.vertical = 0
                    lights.horizontal = 0
                    flag = 1
                    next_green = 'horizontal'
                else:
                    lights.vertical = 0
                    lights.horizontal = 0
                    flag = 1
                    next_green = 'vertical'




        #print(f"Światla wertykalne {lights.vertical}")
        #print(f"Światla horyzontalne {lights.horizontal}")
        chowd(CARS_VERTICAL, BOARD, lights, 1)
        
        chowd(CARS_HORIZONTAL, BOARD, lights, 0)
        BOARD = Board(N)
        #print(f"{counter}------------------------")
        #counter += 1
        time.sleep(1)
        #print(CARS)
        #input()


if __name__ == "__main__":
    main()
