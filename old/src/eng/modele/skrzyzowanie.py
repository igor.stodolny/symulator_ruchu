import random
import time
#from src.eng.car import Car, direction
import numpy as np
from enum import Enum

def gen_color():
    r = random.randint(50, 255)
    g = 0
    b = random.randint(50, 255)
    return (r, g, b)



class direction(Enum):
    up = 8
    down = 2
    right = 6
    left = 4
N = 11
CARS = []
showup_chance = 4
class Spot:
    def __init__(self, category):
        self.category = category
        self.v_max = 0
    def __repr__(self):
        return str(self.category)
    
class Board:
    def __init__(self, N):
        if N % 2 == 0:
            N += 1
        self.BOARD = [[Spot(0) for j in range(N)] for i in range(N)]
        for i, row in enumerate(self.BOARD):
            for j, col in enumerate(row):
                if(i == len(self.BOARD)//2):
                    for elem in row:
                        elem.category = 1
                        elem.v_max = 4
                if(j == len(self.BOARD[0])//2):
                    self.BOARD[i][j].category = 1
                    self.BOARD[i][j].v_max = 4
        self.BOARD[N//2][N//2].category = 2
        self.BOARD[N//2][N//2].v_max = 4
    def __repr__(self):
        res = ""
        for i in self.BOARD:
            res += str(i) + '\n'
        return res

    def update(self, car):
        
        self.BOARD[car.i][car.j] = 3
    
class Car:
    exit_positions = [(0, 10)]
    start_positions = [(0, 5)]

    # Static
    def __init__(self, i, j, drive_direction ,destination, vel = 0):
        assert destination < len(Car.exit_positions), "Destination out of bounds"
        assert isinstance(vel, int), "Velocity must be an int"
        assert isinstance(i, int), "i must be an int"
        assert isinstance(j, int), "j must be an int"
        assert isinstance(drive_direction, direction) , "Direction must be an class enum direction"


        self.color: int = gen_color()
        self.i: int = i
        self.j: int = j

        self.vis_i = self.i
        self.vis_j = self.j
        
        self.destination = destination
        self.vel: float = vel
        self.length = 4
        self.drive_direction = drive_direction

    def move(self):
        if self.drive_direction == direction.right:
            self.j += self.vel
        elif self.drive_direction == direction.left:
            self.j -= self.vel
        elif self.drive_direction == direction.up:
            self.i -= self.vel
        elif self.drive_direction == direction.down:
            self.i += self.vel
    

    def __repr__(self):
        #return str(self.vel)
        return f"{self.i},{self.j}"

    def get_velocity(self):
        return self.vel
    
    def set_velocity(self, new_velocity):
        assert isinstance(new_velocity, int), "Velocity must be an int"
        self.vel = new_velocity

    def get_direction(self):
        return self.drive_direction
    
    def set_direction(self, new_direction):
        assert isinstance(new_direction, direction) , "Direction must be an class enum direction"
        self.drive_direction = new_direction

def chowd(cars, board):
    for i in range(len(cars)-1,-1,-1):
        # decider_speedup = random.randrange(speedup_chance)
        # decider_slowdown = random.randrange(slowdown_chance)
        # decider_rulebreak = random.randrange(rulebreak_chance)

        # if decider_rulebreak in [0] and (cars[i].vel >= v_max and cars[i].vel < 10):
        #     cars[i].vel += 1
        # if decider_speedup in [0] and cars[i].vel < v_max:
        #     cars[i].vel += 1
        # if decider_slowdown in [0] and cars[i].vel > 1:
        #     cars[i].vel -= 1


        if(cars[i].drive_direction == direction.right):
            if cars[i].j + cars[i].vel < len(board.BOARD[0]):
                if i!=len(cars)-1:
                    if cars[i].j + cars[i].vel < cars[i+1].j:
                        cars[i].j += cars[i].vel
                    else:
                        cars[i].vel = max(np.abs(cars[i+1].j - cars[i].j - 1), 0)
                        cars[i].j += cars[i].vel
                else:
                    cars[i].j+=cars[i].vel
            else:
                cars.pop()
        else:
            if cars[i].i - cars[i].vel >= 0:
                if i!=len(cars)-1:
                    if cars[i].i - cars[i].vel >= cars[i+1].i:
                        cars[i].i -= cars[i].vel
                    else:
                        cars[i].vel = max(cars[i].i - cars[i+1].i - 1, 0)
                        cars[i].i -= cars[i].vel
                else:
                    cars[i].i-=cars[i].vel
            else:
                cars.pop()

        


    
    decider_showup = random.randrange(showup_chance)
    
    #
    # if decider_showup in [0]:
    #     cars.insert(0, Car(i=5, j=0, drive_direction=direction.right, destination=0, vel=1))
    #     board.update(cars[0])
    # return cars





def main():
    # CARS.insert(0, Car(i=5, j=0, drive_direction=direction.right, destination=0, vel=3))
    # CARS.insert(1, Car(i=5, j=5, drive_direction=direction.right, destination=0, vel=1))
    CARS.insert(0, Car(i=10, j=5, drive_direction=direction.up, destination=0, vel=3))
    CARS.insert(1, Car(i=5, j=5, drive_direction=direction.up, destination=0, vel=1))
    BOARD = Board(N)
    couter = 0
    while True:
        #print(CARS)
        chowd(CARS, BOARD)
        for car in CARS:
            BOARD.update(car)
        print(BOARD)
        BOARD = Board(N)
        print(f"{couter}------------------------")
        couter += 1
        #time.sleep(1)
        input()
if __name__ == "__main__":
    main()