import random
import time

from src.eng.car import Car, direction
import math

class SygnalizacjaSwietlna:
    def __init__(self, road_len, Vmax, ts_green = 6, ts_red = 4):
        self.cars = [] # Array of cars in road
        self.v_max = Vmax # Max legal speed
        self.vvmax = Vmax + 2 # Max speed
        self.ts_green = ts_green # Green light is n ticks
        self.ts_red = ts_red # Red light is n ticks
        self.road_len = road_len # Road len
        self.traffic_light = math.ceil(road_len/2) # Traffic light is in the half way
        self.traffic_light_green = True # Set traffic light, True -> green(go), False -> red (stop)
        
        self.breaking_road_rules = 0.2 # Chance to break the rules and drive with vvmax
        self.car_spawn_ratio = 0.7 # Chance to spawn new car (only if first cell is empty)
        self.random_event = 0.15 # Chance to spawn random event on road (car reduce speed of 1)

        self.t = 0 # Which time it is (number of ticks)

        assert self.ts_green > 0, "Traffic lights minimum time change must be greater than 0."
        assert self.ts_red > 0, "Traffic lights minimum time change must be greater than 0."
        assert self.v_max > 0, "Vmax must be greater than 0"
        assert self.road_len >= 10, "Road len must be greater or equal than 10"

    # Check if first cell in road is empty
    def __first_cell_is_empty(self):
        if len(self.cars) == 0: # If theres no car, then return true
            return True
        for Car in self.cars:
            if Car.j == 0:
                return False # If some car is on first cell return False
        return True # Otherwise return True

    # Return array of cars in road
    def get_cars(self):
        return self.cars

    # Set new value of chance to spawn random event on the road (0 < new_ratio <= 1.0)
    def set_random_event(self, new_ratio):
        assert new_ratio > 0 and new_ratio <= 1, "New ratio must be greater than 0 and less or equal than 1"
        self.random_event = new_ratio

    # Set new value of chance to break the rules (0 < new_ratio <= 1.0)
    def set_breaking_road_rules(self, new_ratio):
        assert new_ratio > 0 and new_ratio <= 1, "New ratio must be greater than 0 and less or equal than 1"
        self.breaking_road_rules = new_ratio
    
    # Set new value of chance to spawn new car (0 < new_ratio <= 1.0)
    def set_car_spawn_ration(self, new_ratio):
        assert new_ratio > 0 and new_ratio <= 1, "New ratio must be greater than 0 and less or equal than 1"
        self.car_spawn_ratio = new_ratio

    # One tick (move cars)
    def tick(self):
        # Generate new car
        if self.car_spawn_ratio > random.random() and self.__first_cell_is_empty():
            self.cars.insert(0, Car(0, 0, direction.right, 0))
        
        # Accelerate
        for i in range(len(self.cars)):
            if (self.cars[i].vel <= self.v_max) or (self.cars[i].vel <= self.vvmax and self.breaking_road_rules > random.random()):
                self.cars[i].vel += 1

        # Breaking
        for i in range(len(self.cars)):
            # This car
            this_car = self.cars[i]
            #Check if there a car in fron
            if i + 1 < len(self.cars):
                # Next car (the closest one)
                next_car = self.cars[i + 1]
                # Distance between next_car and this_car
                di = next_car.j - this_car.j - 1
                # If velocity is greater than distance -> reduce speed to di
                if this_car.vel > di:
                    this_car.vel = di
            # Distance between car and traffic light
            si = self.traffic_light - this_car.j - 1
            if self.traffic_light_green == False and si >= 0 and this_car.vel > si :
                this_car.vel = si
        
        # Random event
        for i in range(len(self.cars)):
            this_car = self.cars[i]
            if this_car.vel > 0 and random.random() < self.random_event:
                this_car.vel -= 1

        # Move cars:
        for i in range(len(self.cars)):
            self.cars[i].move()
        
        # Remove cars out of range
        while len(self.cars) > 0 and self.cars[-1].j >= self.road_len:
            self.cars.remove(self.cars[-1])

        # Increment counter of ticks
        self.t += 1

        # Every ts_green tick's change traffic light color to red
        if self.traffic_light_green == True:
            if self.t % self.ts_green == 0:
                #Change light to red
                self.traffic_light_green = False
        
        elif self.traffic_light_green == False:
            if self.t % self.ts_red == 0:
                #Change light to green
                self.traffic_light_green = True
        
        return self.cars
        
    # Generate and print road(_) with cars(X) in console, where X is velocity
    def print_road(self):
        road = []
        for i in range(self.road_len):
            road.append('_')
        for Car in self.cars:
            assert Car.j < len(road), "Number of cars can't be greater than road len."
            road[Car.j] = str(Car.vel)
        print(road)