import numpy as np

from enum import Enum
class road_type(Enum):
    grass = 0
    street = 1,
    green_light = 2,
    red_light = 3,
    black_tiles = 4,
    white_frame = 5,
    gray_pole = 6
