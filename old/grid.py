import pygame

def grid(window, SIZE, ROWS):

    distanceBtwRows = SIZE // ROWS
    x = 0
    y = 0
    for row in range(ROWS):
        x+= distanceBtwRows
        y += distanceBtwRows

        pygame.draw.line(window, (0,0,0), (x, 0), (x, SIZE))

        pygame.draw.line(window, (0,0,0), (0, y), (SIZE, y))




def redraw(window):
    global SIZE, ROWS

    window.fill((255, 255, 255))
    grid(window, SIZE, ROWS)

    pygame.display.update()



def main():
    global SIZE, ROWS
    SIZE = 500
    ROWS = 25

    window = pygame.display.set_mode((SIZE, SIZE))



    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()
        redraw(window)



main()